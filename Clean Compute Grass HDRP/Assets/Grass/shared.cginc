#ifndef SHARED_FOO
#define SHARED_FOO

struct DrawVertex
{
    float3 positionWS; // The position in world space
    float2 uv;
    float3 diffuseColor;
};

struct DrawTriangle
{
    float3 normalOS;
    DrawVertex vertices[3]; // The three points on the triangle
};

StructuredBuffer<DrawTriangle> _DrawTriangles;

struct v2f
{
    float4 positionCS : SV_POSITION; // Position in clip space
    float2 uv : TEXCOORD0; // The height of this vertex on the grass blade
    float3 positionWS : TEXCOORD1; // Position in world space
    float3 normalWS : TEXCOORD2; // Normal vector in world space
    float3 diffuseColor : COLOR;
};

            // Properties
float4 _TopTint;
float4 _BottomTint;
float _AmbientStrength;

float _ShadowReceiveStrength;
float _Fade;

float3 _MainLightDirection;
float _MainLightAttenuation;
float3 _MainLightColor;


            // The vertex shader definition with properties defined in the Varyings 
            // structure. The type of the vert function must match the type (struct)
            // that it returns.
v2f Vert(uint vertexID : SV_VertexID)
{ // Initialize the output struct
    v2f output = (v2f) 0;

                // Get the vertex from the buffer
                // Since the buffer is structured in triangles, we need to divide the vertexID by three
                // to get the triangle, and then modulo by 3 to get the vertex on the triangle
    DrawTriangle tri = _DrawTriangles[vertexID / 3];
    DrawVertex input = tri.vertices[vertexID % 3];

              //  output.positionCS = TransformWorldToHClip(input.positionWS);
               // output.positionCS = mul(UNITY_MATRIX_P, float4(input.positionWS, 1.0f));
               // output.positionCS = UnityObjectToClipPos(input.positionWS);
               
    output.positionCS.xyz = TransformObjectToWorld(input.positionWS.xyz);
    output.positionCS.xyz = TransformWorldToView(output.positionCS.xyz);
    output.positionCS = mul(UNITY_MATRIX_P, float4(output.positionCS.xyz, 1.0f));


    output.positionWS = input.positionWS;

    float3 faceNormal = _MainLightDirection * tri.normalOS;
    output.normalWS = TransformObjectToWorldNormal(faceNormal, true);
    output.uv = input.uv;

    output.diffuseColor = input.diffuseColor;

    return output;
}

           // The fragment shader definition.            
half4 Frag(v2f i) : SV_Target
{
  // For Shadow Caster Pass
#ifdef SHADERPASS_SHADOWCASTER
            return 0;
#else
            // For Color Pass

#if SHADOWS_SCREEN
                // Defines the color variable
                half4 shadowCoord = ComputeScreenPos(i.positionCS);
#else
    half4 shadowCoord = (i.positionWS, 1);
#endif  
#if _MAIN_LIGHT_SHADOWS_CASCADE || _MAIN_LIGHT_SHADOWS
                Light mainLight = GetMainLight(shadowCoord);
#else
   // Light mainLight = GetMainLight();
#endif
    float shadow = 0.2;
    shadow = saturate((1 - _ShadowReceiveStrength) + shadow);
            // extra point lights support
          //  float3 extraLights;
        //    int pixelLightCount = GetAdditionalLightsCount();
        //    for (int j = 0; j < pixelLightCount; ++j) {
       //         Light light = GetAdditionalLight(j, i.positionWS, half4(1, 1, 1, 1));
       //         float3 attenuatedLightColor = light.color * (light.distanceAttenuation * light.shadowAttenuation);
      //          extraLights += attenuatedLightColor;
       //     }
    float4 baseColor = lerp(_BottomTint, _TopTint, saturate(i.uv.y)) * float4(i.diffuseColor, 1);

            // multiply with lighting color
    float4 litColor = (baseColor * float4(1, 1, 1, 1));

       //     litColor += float4(extraLights,1);
            // multiply with vertex color, and shadows
    float4 final = litColor * shadow;
            // add in basecolor when lights turned down
    final += saturate((1 - shadow) * baseColor * 0.2);
            // fog
          //  float fogFactor = i.fogFactor;

            // Mix the pixel color with fogColor. 
          //  final.rgb = MixFog(final.rgb, fogFactor);
            // add in ambient color
       //     final += (unity_AmbientSky * _AmbientStrength);

            // fade to bottom transparency
#if FADE
                float alpha = lerp(0, 1, saturate(i.uv.y * _Fade));
                final.a = alpha;
#else
    final.a = 1;
#endif
    return final;

#endif  // SHADERPASS_SHADOWCASTER
}

#endif // SHARED_FOO